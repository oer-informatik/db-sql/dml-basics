# DELETE Statements - Data Manipulation Language (SQL-DML)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-dml-delete</span>

> **tl/dr;** _Delete-Statements löschen selektierte Datensätzen. Sie gehören zur DML-Sprachfamilie, lassen also die Datenstruktur (z.B. auch Indexnummern) unangetastet._

Die Sprachbestandteile der SQL-DML bilden die Methoden für *C*reate, *R*ead, *U*pdate, *D*elete. Letzteres wird mit dem Befehl `DELETE` umgesetzt, der folgende allgemeine Form hat:

```sql
DELETE FROM
nameDerTabelle
WHERE bedingung;
```

An einem praktischen Beispiel:

```sql
DELETE FROM
adressen
WHERE name LIKE 'Hannes';
```

Für den `WHERE`-_Clause_ gelten die gleichen Voraussetzungen, wie beim `SELECT`-Statement. Der `WHERE`-_Clause_ ist optional, wird er weggelassen werden alle Datensätze der Tabelle gelöscht. Die _Autoincrement_-Werte werden jedoch nicht zurückgesetzt (hierfür gibt es weitere Befehle, etwa `TRUNCATE`, was aber Bestandteil der DDL ist).

Als Railroad-Diagramm dargestellt ist die allgemeine Syntax des `DELETE`-Statements:

![Railroad-Diagramm für DELETE](images/sql-dml-railroad-delete-oneline.png)

Sofern die Datenbank händisch bearbeitet wird, bietet es sich an, zunächst per `SELECT`-Befehl zu prüfen, ob die Bedingung des `WHERE`-_Clauses_ auch nur die gewünschten Zeilen betrifft:

```sql
SELECT *  FROM adressen WHERE name LIKE 'Hannes';
```

Trifft dies nach Kontrolle zu kann das `SELECT *` durch ``DELETE` ersetzt werden. Auf diesem Weg lassen sich schwerwiegende Folgen von Flüchtigkeitsfehler minimieren.

```sql
DELETE  FROM adressen WHERE name LIKE 'Hannes';
```

## Links und weitere Informationen
- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/Data_structure_definition/Data_types/Numeric_types)

