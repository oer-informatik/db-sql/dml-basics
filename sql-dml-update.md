# UPDATE Statements - Data Manipulation Language (SQL-DML)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-dml-update</span>

> **tl/dr;** _Update-Statements ändern den Inhalt von selektierten Datensätzen. Die Werte können aus Literalen, Subqueries oder Formeln stammen._

Ein Update-Befehl besteht aus vier Komponenten, wobei die letzte Zeile optional ist:

```sql
UPDATE
tableName
SET columnName='value'
WHERE expression;
```

Im Einzelnen:

- Schlüsselwort `UPDATE`,

- `tableName`: der Name der zu ändernden Tabelle

- nach dem Schlüsselwort `SET` die Zuweisungsbeziehung (`columnName = value`), die ausgeführt werden soll. Sollten mehrere Zuweisungen durchgeführt werden, werden diese kommagetrennt aneinandergefügt.

- optional die Selektion (`WHERE`), bei welchen Datensätzen die Zuweisung durchgeführt werden soll. Einstellungen des DBMS oder Frontends können hier verlangen, dass ein `key`-Attribut im `WHERE`-Clause genannt wird (_safe mode_).


![Railroad-Diagramm für UPDATE](images/sql-dml-railroad-update-oneline.png)



Der Name der Tabelle, der Name des zu aktualisierenden Attributs, der neue Wert des Attributs und eine Bedingung, für welche Datensätze die Änderung erfolgen soll (Selektion).

Es können mehrere Attribute zeitgleich aktualisiert werden, wenn die key/value-Paare kommagetrennt gelistet werden:

```sql
UPDATE
nameDerTabelle
SET attribut1='Tim Mustermann', attribut2=23123
WHERE ID = 123;
```

Vorsicht: wenn die Bedingung weggelassen wird, wird die Änderung für alle Datensätze übernommen. Das ist jedoch nur in den wenigsten Fällen gewünscht.

```sql
UPDATE
nameDerTabelle
SET attribut1='Tim Mustermann', attribut2=23123;
```

Daher empfiehlt es sich bei händischer Bearbeitung der Datenbank immer den `WHERE`-_Clause_ einmal vorneweg mit einem `SELECT`-Statement zu überprüfen.

Beispiel: es sollen alle Datensätze mit dem Schreibfehler 'Mcihael' angepasst werden. Welche Zeilen sind betroffen?

```sql
SELECT * FROM adressen
WHERE vorname='Mcihael';
```

Das `SELECT * FROM` wird dann durch `UPDATE` ersetzt, danach der `SET`-_Clause_ eingefügt:

```sql
UPDATE
adressen
SET vorname='Michael'
WHERE vorname='Mcihael';
```

Auf diese Art lassen sich einfach Fehler bei händischer Anpassung vermeiden.

Als zu aktualisierender Wert in Update-Befehlen können auch _Single-Row-Subquery_-Ergebnisse verwendet werden.

Sofern als Wert eines Updates der ursprüngliche DEFAULT-Wert genutzt werden soll, kann dies ebenso explizit angegeben werden:

```sql
UPDATE
nameDerTabelle
SET attribut1='Tim Mustermann', attribut2=DEFAULT;
```

Die Zuweisung kann auch selbstreferenziell erfolgen. Eine Preiserhöhung um 10% lässt sich also folgendermaßen umsetzen:

```sql
UPDATE
artikelliste
SET preis=preis * 1.1
WHERE preiserhoehung is true
```

## Links und weitere Informationen

- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/Data_structure_definition/Data_types/Numeric_types)


