# INSERRT Statements - Data Manipulation Language (SQL-DML)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-dml-insert</span>

> **tl/dr;** _Insert-Statements sind zuständig dafür, neue Datensätze in die Tabellen einzufügen. Als Datenquelle können Literale oder `SELECT`-Statements dienen._

Die Data Manipulation Language (DML) ist zuständig für die Veränderungen der _Inhalte_ von Tabellen, im Gegensatz zur _Data Definition Language_, die die _Struktur_ der Tabellen definiert.

## Allgemeine Form des `INSERT`-Statements

Ein SQL-INSERT-Statement wird in der allgemeinen Form folgendermaßen notiert:

```sql
INSERT INTO
nameDerTabelle
(nameAttribut1, nameAttribut2, nameAttribut3)
VALUES (wert1, wert2, wert3);
```

Beispiel:

```sql
INSERT INTO
adressen
(Name, Strasse, Hausnummer)
VALUES ('Max Mustermann', 'Hauptstr.', 3);
```
Die vereinfachte Syntax lässt sich mit ihren Optionalitäten und Wiederholungen in folgendem Railroad-Diagramm darstellen:

![Railroad-Diagramm für INSERT](images/sql-dml-railroad-insert.png)

Alle Namen der einzufügenden Attribute werden in Klammern kommagetrennt gelistet. Die Attributwerte werden in der zweiten Klammer in identischer Reihenfolge - bei Zeichenfolgen in Anführungszeichen - gelistet.


## Umgang mit Default-Werten

Alle Attribute, die nicht explizit in der Attributliste genannt werden, werden mit `NULL`-Werten befüllt, sofern kein `DEFAULT`-Wert in der Tabellendefinition (DDL) angegeben wurde.

Sofern dieses Verhalten explizit ausgedrückt werden soll (oder die Kurzschreibweise gewählt wurde) kann als zu speichernder Wert DEFAULT angegeben werden:

```sql
INSERT INTO
nameDerTabelle
(attribut1, attribut2, attribut3)
VALUES (1, ‘Tim Mustermann’, DEFAULT) ;
```

## Umgang mit `NULL`

In der Informationstechnik wird zwischen leeren Feldern (leere Zeichenketten), dem Wert `0` und nichts (`NULL`) unterschieden. Für alle drei Varianten "nichts" einzufügen gibt es eine SQL-Notation:

```sql
INSERT INTO nameDerTabelle
VALUES (NULL, '', 0) ;
```

Die einzelnen DBMS bieten Möglichkeiten, direkt spezielle Werte (heutiges Datum, Benutzername, ...) anzugeben.

## Einfügen mehrerer Datensätze

Es können mehrere Datensätze mit einem Befehl übergeben werden, denn die in Klammern eingefassten Datensätze kommagetrennt aneinandergefügt werden:

```sql
INSERT INTO
adressen
(Name, Strasse, Hausnummer)
VALUES ('Max Mustermann', 'Hauptstr.', 3),
('Bertram Beispiel', 'Goethestr.', 10),
('Petra Platzhalter', 'Adenauerplatz', 15);
```

## Einfügen der Daten aus einer Abfrage

Sofern bereits Tabellen mit den gesuchten Daten vorhanden sind lässt sich auch direkt das Ergebnis einer `SELECT`-Abfrage in die neue Tabelle per Subquery einfügen.

```sql
INSERT INTO
nameDerTabelle
(attribut1, attribut2, attribut3)
SELECT attributA, attributB, attributC
FROM nameDerAnderenTabelle ;
```

Wichtig ist nur, dass die Struktur und Reihenfolge der Felder des `SELECT`-Statements exakt der Attributliste des `INSERT`-Statements entspricht. Natürlich kann das `Select`-Statement auch komplexer aufgebaut sein.

Sollten die Strukturen (Reihenfolge der Attribute) in beiden Tabellen identisch sein, lässt es sich über eine Kurzform noch weiter vereinfachen:

```sql
INSERT INTO
nameDerTabelle
SELECT *
FROM nameDerAnderenTabelle ;
```

## Attributnamen in Ausnahmefällen obsolet

Sofern die Attributwerte im `INSERT`-Statement in exakt der Reihenfolge genannt werden, in der Sie per `SQL-DDL` festgelegt wurden, kann die Liste der Attributnamen auch weggelassen werden. Die Reihenfolge kann man z.B.: mit `DESCRIBE tabellenname` nachschlagen. Nicht alle DBMS unterstützen diese Kurzform, zudem ist sie fehleranfällig. Daher sollte sie nur in begründeten Ausnahmefällen genutzt werden.

```sql
INSERT INTO
nameDerTabelle
VALUES (1, 'Tim Mustermann', 2500);
```

## Links und weitere Informationen
- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/Data_structure_definition/Data_types/Numeric_types)

